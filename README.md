# **Finding Lane Lines on the Road** 

---

The goals of this project were to:
* Make a pipeline that finds lane lines on the road
* Filter detected line segments to map out the full extent of the lane lines
* Reflect on the work in a written report

---

### Reflection

### 1. Pipeline Description

The pipeline consisted of 6 steps:
 
 * Convert the image to grayscale
 * Add a Gaussian blur to remove rough edges
 * Use Canny edge detection to get all object borders
 * Mask the image to ignore data in the upper half
 * Use a Hough Lines transform to detect straight lines from the detected edges
 * Sort the lines into the left and right sides and calculate an average


I modified the draw_line() function by first sorting the Hough lines into the left and right sides by their location in the image. I also discarded any lines with a shallow slope. For the left and right sides, I calculated an average line and extended it to cover the lane markers in the original image.



### 2. Potential Shortcomings


There are some limitations with this solution. It's been calibrated for the images provided and isn't adaptable to different conditions (road types, weather, etc...). 

Additionally, the big lines are based on averages and are noisy in videos which causes them to move around erratically.


### 3. Possible Improvements

I attempted to improve the final lines by filtering outlying Hough lines. Unfortunately, this didn't improve the consistency of the final lines in the video examples. However, I believe that this approach could provide improvements with more effort.

Another improvement would be to add temporal awareness for videos. By using a rolling median location or a Kalman filter, the lane markings can be made more smooth over time.
